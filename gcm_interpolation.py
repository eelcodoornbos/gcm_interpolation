import glob
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import netCDF4

kmfactor = {'cm': 1e-5,
            'm': 1e-3,
            'km': 1}

class ncdataset():
    # Class to access NetCDF data files that are output of the WACCM-X or TIE-GCM
    # general circulation models of the thermosphere-ionosphere

    def __init__(self, ncpath):
        '''Initialize the class by providing ncpath, which is a string containing the
        file system path to a folder containing one or more NetCDF files with .nc extension,
        which are the files used for interpolation. The class assumes that these .nc files
        are the output of the same model run, so that they contain continuous interval
        time stamps, the same variables, and on the same coordinate grid.'''
        self.isvalid = False
        self.ncpath = ncpath
        self.files = glob.glob(f"{self.ncpath}/*.nc")
        if len(self.files) > 1:
            self.isvalid = True
            self.setup_coordinates(roundtime='min')
            self.ncdata = None
            self.ncopenfile = None

    def setup_coordinates(self, roundtime=False):
        '''Stores the latitude and longitude coordinates as well as the timestamps
        with their indices and filenames, found in the (series of) NetCDF file(s).
        The roundtime argument can be used to round the Pandas timestamps to the
        nearest Pandas time interval (e.g. '1min' or '1H'), in case the time stamps
        would otherwise look ugly when printed. This might happen because the time is
        stored in fractional days in model output versions.'''

        # Take lats and lons arrays from any file, in this case first file in the list.
        #  The code assumes that all files are from the same model, so share the same
        #  lat / lon structure
        with netCDF4.Dataset(self.files[0]) as ncdata:
            self.variables = ncdata.variables
            self.variablenames = ncdata.variables.keys()
            # Find ('time', 'lev', 'lat', 'lon') variables
            self.vars4d = []
            self.zvars = []
            for var in self.variablenames:
                if ncdata.variables[var].dimensions == ('time', 'lev', 'lat', 'lon'):
                    self.vars4d.append(var)
                if var.startswith('Z'):
                    self.zvars.append(var)

            self.lats = ncdata.variables['lat'][:]
            self.lons = ncdata.variables['lon'][:]
            self.levs = ncdata.variables['lev'][:]

            # Units and other metadata
            self.levunit = ncdata.variables['lev'].units
            self.zvarunits = {}
            self.zvarkmfactor = {}
            self.heights = {}
            for var in self.zvars:
                unit = ncdata.variables[var].units
                self.zvarunits[var] = unit
                self.zvarkmfactor[var] = kmfactor[unit]
                self.heights[var] = [np.mean(ncdata.variables[var][0,i,:,:]) for i in range(len(self.levs))]

        self.latstep = self.lats[1] - self.lats[0]
        self.lonstep = self.lons[1] - self.lons[0]

        # Set up empty lists for the time stamp variables, to be filled later
        timelist = []
        indexlist = []
        filelist = []

            
        # Loop over the files to find the time stamps, the file in which each
        #  time stamp can be found, and the corresponding time index
        files = sorted(self.files)
        for file in files:
            with netCDF4.Dataset(file) as ncdata:
                times = ncdata['time']
                # The time units are of the form: "minutes since 2015-03-15 00:00:00"
                # the code below makes sure that the times are stored accordingly in the
                # form of pandas timestamps.
                time_unit_components = times.units.split('since')
                time_unit = time_unit_components[0].strip()
                time0 = pd.to_datetime(time_unit_components[1])
                for time_index, time_offset in enumerate(times[:]):
                    timelist.append(time0 + pd.to_timedelta(time_offset, time_unit))
                    indexlist.append(time_index)
                    filelist.append(file)
            
        # Set up the pandas dataframe with the info found above
        df = pd.DataFrame(index=timelist, data={'file': filelist, 'time_index': indexlist})
        self.timestep = timelist[1] - timelist[0]

        # Round the time stamps, if necessary (because conversion from fraction
        #  of day to minutes might make some time stamps look ugly)
        if roundtime:
            df.index = df.index.round(roundtime)
            
        # Store the dataframe in the object instance
        self.ncfiles_for_time = df
        
    def time_interpolation_indices(self, time):
        '''For a certain time, find the files, indices within the files and
        interpolation weights, that allow interpolation for that time'''
        timestep = self.timestep        
        numfiles = len(self.ncfiles_for_time.index)
        index = np.searchsorted(self.ncfiles_for_time.index, pd.to_datetime(time))

        # TODO: sort out what to do when an interpolation time stamp does not belong inside 
        # the date range of the available NetCDF data files

        t0 = self.ncfiles_for_time.iloc[index-1].name        
        t1 = self.ncfiles_for_time.iloc[index].name

        file0 = self.ncfiles_for_time.iloc[index-1].file
        file1 = self.ncfiles_for_time.iloc[index].file

        index0 = self.ncfiles_for_time.iloc[index-1].time_index
        index1 = self.ncfiles_for_time.iloc[index].time_index        

        weight1 = (time - t0) / timestep
        weight0 = 1.0 - weight1

        files = (file0, file1)
        indices = (index0, index1)
        weights = (weight0, weight1)

        return files, indices, weights

    def lat_lon_interpolation_indices(self, lat, lon):
        '''For a certain latitude/longitude combination, find the indices and
        weights that allow linear interpolation'''

        lats = self.lats
        lons = self.lons

        latstep = self.latstep
        lonstep = self.lonstep

        nlats = len(lats)
        nlons = len(lons)
        
        # Set the latitude indices and weights
        ilat1 = np.searchsorted(lats, lat)
        ilat0 = ilat1 - 1
        
        # Handle edge cases
        if ilat0 < 0: 
            # The point is to the south of the southernmost point
            ilat0 = 0
            ilat1 = 0
            weightlat0 = 0.5
            weightlat1 = 0.5
        if ilat1 > nlats - 1: 
            # The point is to the north of the northernmost point
            ilat0 = nlats -1
            ilat1 = nlats - 1
            weightlat0 = 0.5
            weightlat1 = 0.5
        else: # The point is nicely somewhere in our latitude grid
            lat0 = lats[ilat0]
            weightlat1 = (lat - lat0)/latstep
            weightlat0 = 1 - weightlat1

        # Set the longitude indices and weights
        # First check whether the longitude input is in the right range
        if lons[0] >= 0 and lon < 0:
            lonrange = lon + 360.0
        else:
            lonrange = lon

        # Find where in the longitude range we should look
        ilon1 = np.searchsorted(lons, lonrange)
        ilon0 = ilon1 - 1

        # Handle edge cases
        if ilon0 < 0: # Longitude is below the lowermost bound: wrap with upper
            ilon0 = 0
            ilon1 = nlons - 1
            lon0 = lons[ilon0]
            weightlon1 = (lon0 - lonrange)/lonstep
            weightlon0 = 1 - weightlon1
        if ilon1 > nlons -1: # Longitude is above the uppermost bound: wrap with lower
            ilon0 = 0
            ilon1 = nlons - 1
            lon1 = lons[ilon1]
            weightlon0 = (lonrange - lon1)/lonstep
            weightlon1 = 1 - weightlon0
        else: # The point is nicely somewhere in our longitude grid
            lon0 = lons[ilon0]
            weightlon1 = (lonrange - lon0)/lonstep
            weightlon0 = 1 - weightlon1

        # Set the weights and indices tuples for output
        indices = ((ilat0, ilon0), (ilat0,ilon1), (ilat1, ilon0), (ilat1, ilon1))
        weights = (weightlat0*weightlon0, weightlat0*weightlon1, weightlat1*weightlon0, weightlat1*weightlon1)

        return indices, weights
    
    def lev_profile(self, file, time_index, latlon_indices, heightvar, columnvars):
        '''Provide a numpy array containing a vertical profile at a certain
        latitude/longitude/time gridpoint in the GCM grid. The heightvar contains
        the variable for the index, usually the height as a function of pressure
        level. The columnsvar contains a list of the NetCDF grid variables of interest.'''

        # Open the NetCDF4 file if it is not already open
        if file != self.ncopenfile:
            if self.ncdata: # Close if a file is already open
                self.ncdata.close()
            self.ncdata = netCDF4.Dataset(file)
            self.ncdata.set_always_mask(False)
            self.ncopenfile = file

        # The index of the dataframe, a variable with dimension of lev, such as the altitude
        if heightvar == 'ZG':
            zscale = 1e-5
        else:
            zscale = 1e-3
        index = self.ncdata.variables[heightvar][time_index, :, latlon_indices[0], latlon_indices[1]] * zscale

        # The data of the dataframe, according to the provided columns
        columns_list = np.atleast_1d(columnvars)
        data = np.empty((len(columns_list),len(index)))
        for i, var in enumerate(columns_list):
            if var == 'lev': # Generalize to take into account number of variable dimensions
                data[i] = self.ncdata.variables['lev'][:]
            else:
                data[i] = self.ncdata.variables[var][time_index, :, latlon_indices[0], latlon_indices[1]]
        return index, data
    
    def height_interpolation(self, time, lat, lon, heights, heightvar, columnvars):
        '''Provide a numpy array containing a vertical profile, interpolated at
        exactly specified latitude/longitude and time, and for a range (list) of
        altitudes. The arguments lat and lon must be scalars. Only height can be
        a vector, to get a height profile. The reason for this is that interpolation
        as a function of height is relatively easy, by making use of the interp1d
        function. For latitude and longitude, this is not possible, because the 
        interpolation might have to take place at different pressure levels, for
        different lat/lon grid points.'''

        heightsarr = np.atleast_1d(heights)
        files, time_indices, time_weights = self.time_interpolation_indices(time)
        latlon_indices, latlon_weights    = self.lat_lon_interpolation_indices(lat, lon)
        result_array = np.zeros((len(columnvars), len(heightsarr)))
        for f, ti, tw in zip(files, time_indices, time_weights):
            for lli, llw in zip(latlon_indices, latlon_weights):
                weight = tw * llw
                lev_height, lev_data = self.lev_profile(f, ti, lli, heightvar, columnvars)
                if columnvars == 'lev':
                    interpolator = interp1d(lev_height, np.log(lev_data), axis=1, bounds_error=False, fill_value="extrapolate")                    
                    result_array += np.exp(interpolator(heightsarr)) * weight
                else:
                    interpolator = interp1d(lev_height, lev_data, axis=1, bounds_error=False, fill_value="extrapolate")                    
                    result_array += interpolator(heightsarr) * weight
        return result_array
